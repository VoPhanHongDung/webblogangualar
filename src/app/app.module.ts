import { BrowserModule } from '@angular/platform-browser';
import { NgModule, OnInit } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ArticleComponent } from './components/article/article.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RecentcommentComponent } from './components/recentcomment/recentcomment.component';
import { HomeService } from 'src/app/components/shared/home.service';
import { RecentCommentService } from 'src/app/components/shared/recentcomment.service';
import { RecentArticleComponent } from './components/recent-article/recent-article.component';
import { RecentArticleService } from 'src/app/components/shared/recentarticle.service';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ArticleService } from 'src/app/components/shared/article.service';
import { CommonModule } from '@angular/common';
import { CommentComponent } from './components/comment/comment.component';
import { CommentService } from 'src/app/components/shared/comment.service';
import { CommentPostService } from 'src/app/components/shared/commentpost.service';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { CreateUserService } from './components/shared/create-user.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateUserProfileComponent } from './components/create-user-profile/create-user-profile.component';
import { UserProfileService } from './components/shared/userprofile.service';
import { LoginComponent } from './components/login/login.component';
import { LoginService } from './components/shared/login.service';
import { AuthGuard } from './components/shared/auth-guard.service';
import { ArticlepostComponent } from './components/articlepost/articlepost.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ArticleComponent,
    RecentcommentComponent,
    RecentArticleComponent,
    CommentComponent,
    CreateUserComponent,
    CreateUserProfileComponent,
    LoginComponent,
    ArticlepostComponent,


  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FroalaEditorModule.forRoot(), 
    FroalaViewModule.forRoot(),
 
 
  ],
  providers: [
    HomeService,
    RecentCommentService,
    RecentArticleService,
    ArticleService,
    CommentService,
    CommentPostService,
    CreateUserService,
    UserProfileService,
    LoginService,
    AuthGuard,
   
  ],
  bootstrap: [AppComponent]
})
export class AppModule   {
  
  

  
}
