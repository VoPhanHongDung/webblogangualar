import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LoginService } from '../shared/login.service';
import { User } from '../shared/User.model';
import { Router } from '@angular/router';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[LoginService]
})


export class LoginComponent implements OnInit {
 
  invalidLogin :any;


  constructor(private loginService: LoginService, private router: Router) { 
    
  }
 

  ngOnInit() {
  }

  login(loginForm){

    let user = new User(loginForm.value.username , loginForm.value.password);
    this.loginService.login(user).subscribe(response => {
      let token = (<any>response).token;
      let username = (<any>response).userName;
      let idUserProfile = (<any>response).id;  
      localStorage.setItem("jwt", token);
      localStorage.setItem("username",username);
      localStorage.setItem("idUserProfile", idUserProfile);
      this.invalidLogin = false;
      this.router.navigate(["/home",username]);
      //window.location.reload();
    }, err => {
      this.invalidLogin = true;
    });
  }

  

 

  


}
