import { Injectable } from "@angular/core";
import { Response, headersToString } from "selenium-webdriver/http";
import { Article } from "src/app/components/shared/article.model";
import { Observable } from "rxjs/internal/Observable";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserProfile } from "./userprofile.model";


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};


@Injectable()



export class UserProfileService {
    articleList: any;
    httpBase: string = environment.HttpBase;
    constructor(private http: HttpClient) {

    }

    createUserProfile(userProfile: UserProfile) {
        return this.http.post<Response>(this.httpBase + '/User/RegisterUserProfile', userProfile, httpOptions);
    }

    getUserProfile(id){
        return this.http.get<Response>(this.httpBase + "/User/GetUserProfile/"+id , httpOptions);
    }

}