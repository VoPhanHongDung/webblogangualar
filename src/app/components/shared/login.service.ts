import { Injectable } from "@angular/core";
import { Response, headersToString } from "selenium-webdriver/http";

import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from "./User.model";




const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};


@Injectable()



export class LoginService {
    articleList: any;
    httpBase: string = environment.HttpBase;

    constructor(private http: HttpClient) {
    
    }
  
    login(user : User){
        return this.http.post<Response>(this.httpBase +'/User/Login',user,httpOptions)
    }

    logOut() {
        localStorage.removeItem("jwt");
        localStorage.removeItem("username");
    }

}