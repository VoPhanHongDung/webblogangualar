export class Article {
    Id : number;
    Title : string;
    Content : string;
    IdUserProfile : number;
    DateCreate : Date;

    constructor(title : string , content : string  , idUserPro : number, date : Date)
    {
        this.Title = title,
        this.Content = content, 
        this.IdUserProfile = idUserPro,
        this.DateCreate = date
    }
}