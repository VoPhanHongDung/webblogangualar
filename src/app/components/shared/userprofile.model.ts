

export class UserProfile {
    id : number;
    firstname : string;
    lastname : string;
    email : string;
    address : string;
    idUser : number;    
    constructor(firstname : string , lastname : string , email : string , address : string, idUser : number){
       
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.address = address;
        this.idUser = idUser;
    }
}