import { Injectable } from "@angular/core";
import { Response, headersToString } from "selenium-webdriver/http";
import { Article } from "src/app/components/shared/article.model";
import { Observable } from "rxjs/internal/Observable";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};


@Injectable()



export class ArticleService {
    
    httpBase: string = environment.HttpBase;
    constructor(private http: HttpClient) {

    }

    getArticleDetail(id): Observable<Article> {
        return this.http.get<Article>(this.httpBase + 'Article/GetArticleById/'+id, httpOptions);

    }

    creatArticle(article: Article) {

        let token = localStorage.getItem("jwt");
        const httpOptionsToken = {
            headers: new HttpHeaders({
                "Authorization": "Bearer " + token,
                "Content-Type": "application/json"
            })
        };

        return this.http.post<Response>(this.httpBase + 'Article/CreateArticle', article, httpOptionsToken)

    }




}