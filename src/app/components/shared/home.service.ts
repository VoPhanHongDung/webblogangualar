import { Injectable } from "@angular/core";
import { Response, headersToString } from "selenium-webdriver/http";
import { Article } from "src/app/components/shared/article.model";
import { Observable } from "rxjs/internal/Observable";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};


@Injectable()



export class HomeService  {
    articleList : any;
    httpBase : string = environment.HttpBase;
    constructor(private http : HttpClient) {
        
    }

    getArticleList(): Observable<Article[]> {
        let token = localStorage.getItem("jwt");
        return this.http.get<Article[]>(this.httpBase + 'Home/GetAllArticle', {
            headers: new HttpHeaders({
                "Authorization": "Bearer " + token,
                "Content-Type": "application/json"
            })
        });
  
    }

}