import { Injectable } from "@angular/core";
import { Response, headersToString } from "selenium-webdriver/http";
import { Article } from "src/app/components/shared/article.model";
import { Observable } from "rxjs/internal/Observable";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from "./User.model";


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};


@Injectable()



export class CreateUserService {
    articleList: any;
    httpBase: string = environment.HttpBase;
    constructor(private http: HttpClient) {

    }

    createUser(user: User) {
        return this.http.post<Response>(this.httpBase + '/User/RegisterUser', user,httpOptions);
    }

   

}