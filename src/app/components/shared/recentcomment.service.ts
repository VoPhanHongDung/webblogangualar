import { Injectable } from "@angular/core";
import { Response, headersToString } from "selenium-webdriver/http";
import { Article } from "src/app/components/shared/article.model";
import { Observable } from "rxjs/internal/Observable";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Comments } from "src/app/components/shared/comments.model";



const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};


@Injectable()



export class RecentCommentService {
    httpBase: string = environment.HttpBase;

    constructor(private http: HttpClient) {

    }

    getRecentComment(): Observable<Comments[]> {
        return this.http.get<Comments[]>(this.httpBase + 'Home/GetRecentComment', httpOptions);

    }

}