export class Comments {
    detailComment : string;
    idArticle : number;
    fullName : string;
    dateCreate : Date;

    constructor( detialComment : string , idArt : number , fullName : string, date : Date){
        this.detailComment = detialComment;
        this.idArticle = idArt;
        this.fullName = fullName;
        this.dateCreate = date;
    }
}