import { Injectable } from "@angular/core";
import { Response, headersToString } from "selenium-webdriver/http";
import { Article } from "src/app/components/shared/article.model";
import { Observable } from "rxjs/internal/Observable";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Comments } from "./comments.model";
import { RequestMethod } from "@angular/http";
import { BehaviorSubject } from "rxjs";


@Injectable()


export class CommentPostService {
  
    httpBase: string = environment.HttpBase;
    private messageSource = new BehaviorSubject('default message');
    currentMessage = this.messageSource.asObservable();

    constructor(private http: HttpClient) { }

    creatComment(comments: Comments) {
        
        let token = localStorage.getItem("jwt");
        const httpOptionsToken = {
            headers: new HttpHeaders({
                "Authorization": "Bearer " + token,
                "Content-Type": "application/json"
            })
        };
       
        return this.http.post<Response>(this.httpBase + 'Comment/CreateComment', comments,httpOptionsToken)

    }

    changeMessage(message: string) {
        this.messageSource.next(message)
    }
    
}