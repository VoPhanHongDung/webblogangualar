import { Component, OnInit } from '@angular/core';
import { RecentArticleService } from 'src/app/components/shared/recentarticle.service';
import { Article } from 'src/app/components/shared/article.model';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-recent-article',
  templateUrl: './recent-article.component.html',
  styleUrls: ['./recent-article.component.css'],
  providers: [RecentArticleService]
})
export class RecentArticleComponent implements OnInit {

  recentArticle : Article[];
  imageLink: string = environment.HttpFile;

  constructor(private articleRecentService : RecentArticleService) { }

  ngOnInit() {
    this.articleRecentService.getRecentArticle()
     .subscribe( res => this.recentArticle = res);
  }

}
