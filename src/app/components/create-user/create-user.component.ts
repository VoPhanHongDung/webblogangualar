import { Component, OnInit } from '@angular/core';
import { CreateUserService } from '../shared/create-user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../shared/User.model';
import { Router } from '@angular/router';
import { RegistrationValidator } from '../shared/RegistrationValidator';


@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css'],
  providers: [CreateUserService]
})
export class CreateUserComponent implements OnInit {

  registrationFormGroup: FormGroup;
  passwordFormGroup: FormGroup;
  

  constructor(private formBuilder: FormBuilder , private createUserService : CreateUserService,private router : Router) { 
    this.passwordFormGroup = this.formBuilder.group({
      password: ['', Validators.required],
      repeatPassword: ['', Validators.required]
    }, {
        validator: RegistrationValidator.validate.bind(this)
      });
    this.registrationFormGroup = this.formBuilder.group({
      username: ['', Validators.required],
      passwordFormGroup: this.passwordFormGroup
    });
  }

  ngOnInit() {
  }


  onClickRegister(createUserForm) {

      let user = new User(createUserForm.value.username , createUserForm.value.passwordFormGroup.password);
      this.createUserService.createUser(user).subscribe(
        data => {
          
          console.log("POST Request is successful ", data);
          this.router.navigate(['createuserprofile', data]);
        },
        error => {
          console.log("Error", error);
        }

      );
   

  }

}
