import { Component, OnInit } from '@angular/core';
import { Article } from '../shared/article.model';
import { ArticleService } from '../shared/article.service';
import { UserProfileService } from '../shared/userprofile.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-articlepost',
  templateUrl: './articlepost.component.html',
  styleUrls: ['./articlepost.component.css']
})
export class ArticlepostComponent implements OnInit {

  
  idUserProfile : any;

  public options: Object = {
    // placeholderText: 'Edit Your Content Here!',
    // charCounterCount: false,

    imageUploadURL: 'http://localhost:53498/api/UploadFile',
    // Set request type.
    imageUploadMethod: 'POST',

    events: {
      'froalaEditor.image.uploaded': function (e, editor, response) {
        console.log(response);
      }
    }
  }


  constructor(private articleService : ArticleService , private userProfileService : UserProfileService, private router : Router) { }

  ngOnInit() {
  }

  onSubmit(formPostArticle){
  
    this.idUserProfile = localStorage.getItem("idUserProfile");
  
    console.log(this.idUserProfile)
  
     let article = new Article(
       formPostArticle.value.Title, 
       formPostArticle.value.Content, 
       this.idUserProfile,
       new Date() 
     );

    this.articleService.creatArticle(article).subscribe(
      data => {

        console.log("POST Request is successful ");
        this.router.navigate(["/home"]);
        
      },
      error => {
        console.log("Error", error);
      }
    );
  
  }

}
