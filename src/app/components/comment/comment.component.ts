import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { CommentService } from 'src/app/components/shared/comment.service';
import { Comments } from 'src/app/components/shared/comments.model';
import { CommentPostService } from '../shared/commentpost.service';


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css'],
  providers: [CommentService]
})
export class CommentComponent implements OnInit {

  @Input() idArticle : any;
  message: string;
  commentList : Comments[];
  username : any;
  
  date: any;

  constructor(private commentService : CommentService , private commentPostService : CommentPostService) {
    
   }

  ngOnInit() { 
    this.username = localStorage.getItem("username");
    this.commentService.getCommentList(this.idArticle).subscribe(res =>  this.commentList = res);
  }

  onSubmit(commentForm) {
    this.date = new Date();
    let commentTemp = new Comments(commentForm.value.DetailComment, this.idArticle, this.username, this.date);

    this.commentPostService.creatComment(commentTemp).subscribe(
      data => {

        console.log("POST Request is successful ", commentTemp);
        // this.commentList.push(commentTemp);
        // console.log(this.commentList);
        this.commentService.getCommentList(this.idArticle).subscribe(res => this.commentList = res);
      },
      error => {
        console.log("Error", error);
      }



    );
  }


}
