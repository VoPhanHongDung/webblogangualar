import { Component, OnInit } from '@angular/core';
import { RecentCommentService } from 'src/app/components/shared/recentcomment.service';
import { Comments } from 'src/app/components/shared/comments.model';



@Component({
  selector: 'app-recentcomment',
  templateUrl: './recentcomment.component.html',
  styleUrls: ['./recentcomment.component.css'],
  providers: [RecentCommentService]
})
export class RecentcommentComponent implements OnInit {
  recentComment : Comments[];

  constructor(private recentCommnetService : RecentCommentService) { }

  ngOnInit() {
    this.recentCommnetService.getRecentComment().subscribe(res => 
       this.recentComment = res);
  }

}
