import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ParamMap } from '@angular/router';
import { ArticleService } from 'src/app/components/shared/article.service';
import { Article } from 'src/app/components/shared/article.model';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  idArticle : any;
  articleDetail : Article;


  constructor(private route : ActivatedRoute , private articleService : ArticleService) {
    this.route.paramMap.subscribe((params: ParamMap) => 
    {this.idArticle =  params.get('idArticle');
      this.articleService.getArticleDetail(this.idArticle).subscribe(
        res => this.articleDetail = res
      );
  });
   }

  ngOnInit() {
    
  }

}
