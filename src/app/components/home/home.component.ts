import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/components/shared/article.model';
import { environment } from 'src/environments/environment';
import { HomeService } from 'src/app/components/shared/home.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [HomeService],
})
export class HomeComponent implements OnInit {
  articleList : Article[];
  imageLink : string = environment.HttpFile;
  
  constructor(private homeService : HomeService) { }

  ngOnInit() {
   
    this.homeService.getArticleList().subscribe(res => this.articleList = res);

  }

}
