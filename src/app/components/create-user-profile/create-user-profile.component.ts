import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserProfile } from '../shared/userprofile.model';
import { UserProfileService } from '../shared/userprofile.service';

@Component({
  selector: 'app-create-user-profile',
  templateUrl: './create-user-profile.component.html',
  styleUrls: ['./create-user-profile.component.css']
})
export class CreateUserProfileComponent implements OnInit {

  createUserProfile: FormGroup;
  idUser : any;
  registrationFormGroup :FormGroup;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private userProfileService: UserProfileService, private router: Router) {
    this.route.paramMap.subscribe((params: ParamMap) => this.idUser = params.get('idUser'));
 

    this.createUserProfile = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      //email: ['', Validators.required],
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      address :[''],

    });
 
  }

  ngOnInit() {
  }

  onClickRegister(createUserProfileForm) {
  
    let userProfile = new UserProfile(
      createUserProfileForm.value.firstname,
      createUserProfileForm.value.lastname,
      createUserProfileForm.value.email,
      createUserProfileForm.value.address,
      this.idUser
    );

    this.userProfileService.createUserProfile(userProfile).subscribe(
      data => {

        console.log("POST Request is successful ", data);
        this.router.navigate(['home']);
      },
      error => {
        console.log("Error", error);
      }

    );

  }
}
