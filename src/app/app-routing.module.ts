import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ArticleComponent } from "src/app/components/article/article.component";
import { HomeComponent } from "src/app/components/home/home.component";
import { CreateUserComponent } from "./components/create-user/create-user.component";
import { CreateUserProfileComponent } from "./components/create-user-profile/create-user-profile.component";
import { LoginComponent } from "./components/login/login.component";
import { AuthGuard } from "./components/shared/auth-guard.service";
import { ArticlepostComponent } from "./components/articlepost/articlepost.component";


const routeConfig: Routes = [
    { path: 'articlepost', component: ArticlepostComponent, canActivate : [AuthGuard]},
    { path: 'article/:idArticle', component: ArticleComponent },
    { path: 'createuser', component: CreateUserComponent },
    { path: 'login', component: LoginComponent },
    { path: 'createuserprofile/:idUser', component: CreateUserProfileComponent },
    { path: 'home', component: HomeComponent },//, canActivate : [AuthGuard] },
    { path: '', redirectTo: '/home', pathMatch: 'full' },//default contact
    { path: '**', component: HomeComponent }
];

@NgModule({
    declarations: [
        // ContactComponent,
       

    ],
    imports: [
        RouterModule.forRoot(routeConfig),
     
    ],
    exports: [RouterModule], //remember
    providers: [],
    bootstrap: [],

})

export class AppRoutingModule { }
