import { Component, OnInit, ViewChild, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { LoginService } from './components/shared/login.service';
import { LoginComponent } from './components/login/login.component';
import { Location } from '@angular/common';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {
  title = 'WebBlog-Angular';
  username : string;
 
  
  constructor(private loginService: LoginService ) {
   
  }


  ngOnInit() {
    this.username = localStorage.getItem("username");
  }

  SignOut(){
    this.loginService.logOut();
    this.username = null;
  }
 

  

  

 
}
